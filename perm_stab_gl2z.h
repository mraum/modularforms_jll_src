/*
    Copyright (C) 2021  Albin Ahlbäck, Martin Raum

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PERM_STAB_GL2Z_H
#define PERM_STAB_GL2Z_H

#include "flint/fmpz.h"

typedef struct
{
  /* We encode the stabilizer of the following elements: */
  long int * elts;
  long int nelts;

  /* Permutation of \epsilon = [1 0; 0 -1] */
  long int * epsperm;
  /* Permutation of -I = (S' \epsilon)^2. */
  long int * negperm;
  /* Permutation of S' = [0 1; 1 0]. */
  long int * sperm; /* Super nice name! */
  long int permsize;

  /* We store the cycles decompositions of the action of T = [1 1; 0 1] in
   * order to quickly evaluate powers of T. We sort the permutations in such a
   * way that all cycles are ranges. We store the index in that range, its
   * initial shift (0-based), and its length. */
  long int * tcycls;
  fmpz_t tcycllcm;

  fmpz * cfrac;
  long int cfrac_len;
} perm_stab_gl2z_struct;

typedef perm_stab_gl2z_struct perm_stab_gl2z_t[1];

void perm_stab_gl2z_clear(perm_stab_gl2z_t stab);

void perm_stab_gl2z_init(perm_stab_gl2z_t stab,
        const long int * elts, const long int nelts,
        const long int * epsperm, const long int * negperm,
        const long int * sperm, const long int permsize,
        const long int * tcycls, const fmpz_t tcycllcm);

#endif
