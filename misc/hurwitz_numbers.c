/*
    Copyright (C) 2021  Martin Raum

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <string.h>
#include "flint/nmod_vec.h"


void
hurwitz_numbers(
  mp_ptr hw,
  slong len,
  mp_limb_t modn
  )
{
  nmod_t mod;
  nmod_init(&mod, modn);

  long int d, bd, s, m, n;
  mp_limb_t h;
  mp_limb_t mtwo = nmod_neg(2, mod);

  mp_ptr lambda = _nmod_vec_init(len);
  _nmod_vec_zero(lambda, len);
  mp_ptr sigma = _nmod_vec_init(len);
  _nmod_vec_zero(sigma, len);
  for ( d = 1; d <= len; ++d ) {
    bd = d*d < len ? d*d : len;
    for ( m = 1, n = d; n <= bd ; ++m, n += d ) {
      lambda[n-1] = nmod_add(lambda[n-1], m, mod);
      sigma[n-1] = nmod_add(sigma[n-1], d, mod);
    }
    for ( n = d*(d+1); n <= len; n += d ) {
      /* ERROR HERE at around d=46225 */
      lambda[n-1] = nmod_add(lambda[n-1], d, mod);
      sigma[n-1] = nmod_add(sigma[n-1], d, mod);
    }
  }


  _nmod_vec_zero(hw, len);
  
  if ( len >= 1 ) {
    hw[0] = nmod_neg(1, mod);
    h = nmod_mul(mtwo, hw[0], mod);
    bd = (int)sqrtf(len-1);
    for ( s = 2; s <= bd; s += 2 )
      hw[s*s] = nmod_add(hw[s*s], h, mod);
  }

  if ( len >= 4 ) {
    hw[3] = nmod_add(0, 4, mod);
    h = nmod_mul(mtwo, hw[3], mod);
    bd = (int)sqrtf(len-4);
    for ( s = 1; s <= bd; ++s )
      hw[3+s*s] = nmod_add(hw[3+s*s], h, mod);

    for (m = 1, n = 4; n <= len - 4; ++m, n += 4 ) {
      h = nmod_mul(24, sigma[m-1], mod);
      hw[n] = nmod_add(hw[n], h, mod);
      h = nmod_mul(12, lambda[m-1], mod);
      hw[n] = nmod_sub(hw[n], h, mod);
      h = nmod_mul(mtwo, hw[n], mod);
      if ( n < len ) {
        bd = (int)sqrtf(len-n-1);
        for ( s = 2; s <= bd; s += 2 )
          hw[n+s*s] = nmod_add(hw[n+s*s], h, mod);
      }
  
      h = nmod_mul(4, sigma[n+2], mod);
      hw[n+3] = nmod_add(hw[n+3], h, mod);
      h = nmod_mul(6, lambda[n+2], mod);
      hw[n+3] = nmod_sub(hw[n+3], h, mod);
      h = nmod_mul(mtwo, hw[n+3], mod);
      if ( n+3 < len ) {
        bd = (int)sqrtf(len-n-4);
        for ( s = 1; s <= bd; ++s )
          hw[n+3+s*s] = nmod_add(hw[n+3+s*s], h, mod);
      }
    }

    /* process the remaining cases so that n runs through from 4 to len */
    if ( n < len ) {
      h = nmod_mul(24, sigma[m-1], mod);
      hw[n] = nmod_add(hw[n], h, mod);
      h = nmod_mul(12, lambda[m-1], mod);
      hw[n] = nmod_sub(hw[n], h, mod);
    }
    if ( n + 3 < len ) {
      h = nmod_mul(4, sigma[n+2], mod);
      hw[n+3] = nmod_add(hw[n+3], h, mod);
      h = nmod_mul(6, lambda[n+2], mod);
      hw[n+3] = nmod_sub(hw[n+3], h, mod);
    }
  }

  _nmod_vec_clear(lambda);
  _nmod_vec_clear(sigma);
}
