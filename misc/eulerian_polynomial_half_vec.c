/*
    Copyright (C) 2021  Albin Ahlbäck

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
    Description: Calculate "half of the coefficients" of
    the Eulerian polynomials A_n(x), since c(A_n, m) = c(A_n, n - m - 1).
    We only calculate coefficients 0, 1, ..., floor(n / 2) for A_n(x).
*/

#include "misc.h"

#define SMALL_BOUND (FLINT_BITS == 64 ? 20 : 12)

void
fmpz_vec_eulerian_polynomial_half(fmpz ** res, ulong n)
{
    ulong nx, kx;

    for (nx = 0; nx <= n; nx++)
        fmpz_one(res[nx] + 0);

    for (nx = 3; nx <= n; nx++)
    {
        if (nx <= SMALL_BOUND)
        {
            for (kx = 1; kx <= (nx - 1) / 2; kx++)
                _fmpz_demote(res[nx] + kx);

            if (nx % 2 == 1)
                res[nx][nx / 2] = (nx + 1) * res[nx - 1][nx / 2 - 1];

            for (kx = nx / 2 - 1; kx >= 1; kx--)
                res[nx][kx] = (kx + 1) * res[nx - 1][kx]
                    + (nx - kx) * res[nx - 1][kx - 1];
        }
        else
        {
            if (nx % 2 == 1)
                fmpz_mul_ui(res[nx] + nx / 2, res[nx - 1] + nx / 2 - 1, nx + 1);

            for (kx = nx / 2 - 1; kx >= 1; kx--)
            {
                fmpz_mul_ui(res[nx] + kx, res[nx - 1] + kx, kx + 1);
                fmpz_addmul_ui(res[nx] + kx, res[nx - 1] + (kx - 1), nx - kx);
            }
        }
    }
}
