/*
    Copyright (C) 2021  Albin Ahlbäck

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ARB_WRAPPERS_H
#define ARB_WRAPPERS_H

#include "dirichlet.h"

void dirichlet_char_set_wrapped(dirichlet_char_t x, const dirichlet_group_t G,
                                                    const dirichlet_char_t y);

int dirichlet_char_eq_wrapped(const dirichlet_char_t x,
                                const dirichlet_char_t y);

#endif
