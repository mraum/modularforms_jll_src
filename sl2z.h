/*
    Copyright (C) 2021  Albin Ahlbäck

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SL2Z_H
#define SL2Z_H

#include "flint/fmpz.h"

/*******************************************************************************
 * type
 ******************************************************************************/

typedef struct
{
    fmpz entries[4];
} sl2z_struct;

typedef sl2z_struct sl2z_t[1];

/*******************************************************************************
 * initializations and clears
 ******************************************************************************/

static inline void
sl2z_init(sl2z_t ga)
{
    fmpz_init_set_ui(ga->entries + 0, 1);
    fmpz_init_set_ui(ga->entries + 1, 0);
    fmpz_init_set_ui(ga->entries + 2, 0);
    fmpz_init_set_ui(ga->entries + 3, 1);
}

static inline void
sl2z_init_set(sl2z_t res, const sl2z_t ga)
{
    fmpz_init_set(res->entries + 0, ga->entries + 0);
    fmpz_init_set(res->entries + 1, ga->entries + 1);
    fmpz_init_set(res->entries + 2, ga->entries + 2);
    fmpz_init_set(res->entries + 3, ga->entries + 3);
}

static inline void
sl2z_init_set_si(sl2z_t ga, slong a, slong b, slong c, slong d)
{
    fmpz_init_set_si(ga->entries + 0, a);
    fmpz_init_set_si(ga->entries + 1, b);
    fmpz_init_set_si(ga->entries + 2, c);
    fmpz_init_set_si(ga->entries + 3, d);
}

static inline void
sl2z_init_set_fmpz(sl2z_t ga, const fmpz_t a, const fmpz_t b,
                                   const fmpz_t c, const fmpz_t d)
{
    fmpz_init_set(ga->entries + 0, a);
    fmpz_init_set(ga->entries + 1, b);
    fmpz_init_set(ga->entries + 2, c);
    fmpz_init_set(ga->entries + 3, d);
}

static inline void
sl2z_clear(sl2z_t ga)
{
    fmpz_clear(ga->entries + 0);
    fmpz_clear(ga->entries + 1);
    fmpz_clear(ga->entries + 2);
    fmpz_clear(ga->entries + 3);
}

/*******************************************************************************
 * get and set
 ******************************************************************************/

static inline void
sl2z_set(sl2z_t res, const sl2z_t ga)
{
    fmpz_set(res->entries + 0, ga->entries + 0);
    fmpz_set(res->entries + 1, ga->entries + 1);
    fmpz_set(res->entries + 2, ga->entries + 2);
    fmpz_set(res->entries + 3, ga->entries + 3);
}

static inline void
sl2z_set_fmpz(sl2z_t ga, const fmpz_t a, const fmpz_t b,
                         const fmpz_t c, const fmpz_t d)
{
    fmpz_set(ga->entries + 0, a);
    fmpz_set(ga->entries + 1, b);
    fmpz_set(ga->entries + 2, c);
    fmpz_set(ga->entries + 3, d);
}

static inline void
sl2z_get_a(fmpz_t a, const sl2z_t ga)
{
    fmpz_set(a, ga->entries + 0);
}

static inline void
sl2z_get_b(fmpz_t b, const sl2z_t ga)
{
    fmpz_set(b, ga->entries + 1);
}

static inline void
sl2z_get_c(fmpz_t c, const sl2z_t ga)
{
    fmpz_set(c, ga->entries + 2);
}

static inline void
sl2z_get_d(fmpz_t d, const sl2z_t ga)
{
    fmpz_set(d, ga->entries + 3);
}

static inline void
sl2z_set_a(sl2z_t ga, const fmpz_t a)
{
    fmpz_set(ga->entries + 0, a);
}

static inline void
sl2z_set_b(sl2z_t ga, const fmpz_t b)
{
    fmpz_set(ga->entries + 1, b);
}

static inline void
sl2z_set_c(sl2z_t ga, const fmpz_t c)
{
    fmpz_set(ga->entries + 2, c);
}

static inline void
sl2z_set_d(sl2z_t ga, const fmpz_t d)
{
    fmpz_set(ga->entries + 3, d);
}

/*******************************************************************************
 * miscellanous
 ******************************************************************************/

static inline void
sl2z_one(sl2z_t ga)
{
    fmpz_set_ui(ga->entries + 0, 1);
    fmpz_set_ui(ga->entries + 1, 0);
    fmpz_set_ui(ga->entries + 2, 0);
    fmpz_set_ui(ga->entries + 3, 1);
}

static inline void
sl2z_neg(sl2z_t res, const sl2z_t ga)
{
    fmpz_neg(res->entries + 0, ga->entries + 0);
    fmpz_neg(res->entries + 1, ga->entries + 1);
    fmpz_neg(res->entries + 2, ga->entries + 2);
    fmpz_neg(res->entries + 3, ga->entries + 3);
}

static inline void
sl2z_mul(sl2z_t res, const sl2z_t g, const sl2z_t h)
{
    fmpz_fmma(res->entries + 0, g->entries + 0, h->entries + 0,
                                g->entries + 1, h->entries + 2);
    fmpz_fmma(res->entries + 1, g->entries + 0, h->entries + 1,
                                g->entries + 1, h->entries + 3);
    fmpz_fmma(res->entries + 2, g->entries + 2, h->entries + 0,
                                g->entries + 3, h->entries + 2);
    fmpz_fmma(res->entries + 3, g->entries + 2, h->entries + 1,
                                g->entries + 3, h->entries + 3);
}

static inline void
sl2z_inv(sl2z_t res, const sl2z_t ga)
{
    fmpz_set(res->entries + 0, ga->entries + 3);
    fmpz_neg(res->entries + 1, ga->entries + 1);
    fmpz_neg(res->entries + 2, ga->entries + 2);
    fmpz_set(res->entries + 3, ga->entries + 0);
}

void sl2z_pow_si(sl2z_t res, const sl2z_t ga, slong n);

/*******************************************************************************
 * printing and strings
 ******************************************************************************/

void sl2z_print(sl2z_t ga);

char * sl2z_get_str(char * str, sl2z_t ga);

#endif
