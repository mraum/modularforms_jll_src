/*
    Copyright (C) 2021  Albin Ahlbäck

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sl2z.h"

void
sl2z_print(sl2z_t ga)
{
    flint_printf("[");
    fmpz_print(ga->entries + 0);
    flint_printf(" ");
    fmpz_print(ga->entries + 1);
    flint_printf("; ");
    fmpz_print(ga->entries + 2);
    flint_printf(" ");
    fmpz_print(ga->entries + 3);
    flint_printf("]");
}
