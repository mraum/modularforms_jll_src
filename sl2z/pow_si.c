/*
    Copyright (C) 2021  Albin Ahlbäck

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sl2z.h"

void
sl2z_pow_si(sl2z_t res, const sl2z_t ga, slong n)
{
    sl2z_t tmp;

    sl2z_init(tmp);

    if (n > 0)
        sl2z_set(tmp, ga);
    else
    {
        sl2z_inv(tmp, ga);
        n = -n;
    }

    if (n & 1)
        sl2z_set(res, tmp);
    else
        sl2z_one(res);

    n >>= 1;

    while (n > 0)
    {
        n >>= 1;
        sl2z_mul(tmp, tmp, tmp);
        if (n & 2)
            sl2z_mul(res, res, tmp);
    }
}
