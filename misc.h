/*
    Copyright (C) 2021  Albin Ahlbäck

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODULARFORMS_MISC_H
#define MODULARFORMS_MISC_H

#include "flint/flint.h"
#include "flint/nmod_vec.h"
#include "flint/fmpz.h"
#include "acb.h"

void hurwitz_numbers(mp_ptr hw, slong len, mp_limb_t modn);

void fmpz_vec_eulerian_polynomial_half(fmpz ** res, ulong n);

void
gen_divisorfunction_generatingfunction(
        acb_t res,
        const acb_t tau,
        ulong a,
        ulong b,
        slong prec);

#endif
