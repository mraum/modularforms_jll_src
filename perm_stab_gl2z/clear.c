/*
    Copyright (C) 2021  Martin Raum

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "flint/fmpz.h"
#include "flint/fmpq.h"
#include "perm_stab_gl2z.h"

void
perm_stab_gl2z_clear(
    perm_stab_gl2z_t stab
    )
{
  flint_free(stab->elts);
  fmpz_clear(stab->tcycllcm);
  fmpz_clear(stab->tcycls);
  if ( stab->cfrac )
    _fmpz_vec_clear(stab->cfrac, stab->cfrac_len);
}
