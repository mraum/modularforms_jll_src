/*
    Copyright (C) 2021  Martin Raum

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "flint/fmpz.h"
#include "flint/fmpq.h"
#include "perm_stab_gl2z.h"

void
perm_stab_gl2z_is_element(
    long int * iselem,
    long int * isnegelem,
    perm_stab_gl2z_t stab,
    const fmpz_t a,
    const fmpz_t b,
    const fmpz_t c,
    const fmpz_t d
    )
{
  /* Return as first component whether g = [a b;c d] lies in the stabilizer and
   * as second one whether -g does. */

  /* Special cases of c = 0 and a = 0. */
  if ( fmpz_is_zero(c) ) {
    if ( fmpz_cmp_si(a,0) == 1 ) { /* a > 0 */
      if ( fmpz_is_zero(b) ) { /* b == 0 */
        /* g = I and -g = -I. */
        long int isnegelem_ = 1;
        for ( long int ex = 0; ex < stab->nelts; ++ex ) {
          long int e = stab->elts[ex];
          isnegelem_ &= (e==stab->negperm[e]);
        }
        *iselem    = 1;
        *isnegelem = isnegelem_;

        return;

      } else { /* b != 0 */
        fmpz_t t;
        fmpz_init(t);

        /* g = T^b */
        long int iselem_    = 1;
        long int isnegelem_ = 1;
        for ( long int ex = 0; ex < stab->nelts; ++ex ) {
          long int e = stab->elts[ex];
          long int ox   = stab->tcycls[3*e];
          long int init = stab->tcycls[3*e+1];
          long int len  = stab->tcycls[3*e+2];
          fmpz_add_ui(t, b, (unsigned long int)ox);
          fmpz_mod_ui(t, t, (unsigned long int)len);
          long int ix = fmpz_get_si(t) + init;
          iselem_    &= (e==ix);
          isnegelem_ &= (e==stab->negperm[ix]);
        }
        *iselem    = iselem_;
        *isnegelem = isnegelem_;

        fmpz_clear(t);
        return;
      }

    } else { /* a < 0 */
      if ( fmpz_is_zero(b) ) { /* b == 0 */
        /* g = -I and -g = I. */
        long int iselem_ = 1;
        for ( long int ex = 0; ex < stab->nelts; ++ex ) {
          long int e = stab->elts[ex];
          iselem_ &= (e==stab->negperm[e]);
        }
        *iselem    = iselem_;
        *isnegelem = 1;

        return;

      } else { /* b != 0 */
        fmpz_t t;
        fmpz_init(t);

        /* g = -T^-b */
        long int iselem_    = 1;
        long int isnegelem_ = 1;
        for ( long int ex = 0; ex < stab->nelts; ++ex ) {
          long int e = stab->elts[ex];
          long int ox   = stab->tcycls[3*e];
          long int init = stab->tcycls[3*e+1];
          long int len  = stab->tcycls[3*e+2];
          fmpz_sub_ui(t, b, (unsigned long int)ox);
          fmpz_neg(t, t);
          fmpz_mod_ui(t, t, (unsigned long int)len);
          long int ix = fmpz_get_si(t) + init;
          iselem_    &= (e==stab->negperm[ix]);
          isnegelem_ &= (e==ix);
        }
        *iselem    = iselem_;
        *isnegelem = isnegelem_;

        fmpz_clear(t);
        return;
      }
    }

  } else if ( fmpz_is_zero(a) ) { /* a == 0 */
    if ( fmpz_cmp_si(c,0) == 1 ) { /* c > 0 */
      if ( fmpz_is_zero(d) ) { /* d == 0 */
        /* g = S = S' \epsilon and -g = -S = \epsilon S'. */
        long int iselem_    = 1;
        long int isnegelem_ = 1;
        for ( long int ex = 0; ex < stab->nelts; ++ex ) {
          long int e = stab->elts[ex];
          iselem_    &= (stab->sperm[e]==stab->epsperm[e]);
          isnegelem_ &= (stab->epsperm[e]==stab->sperm[e]);
        }
        *iselem    = iselem_;
        *isnegelem = isnegelem_;

        return;

      } else { /* d != 0 */
        fmpz_t t;
        fmpz_init(t);

        /* g = S' \epsilon T^d and -g = \epsilon S' T^d. */
        long int iselem_    = 1;
        long int isnegelem_ = 1;
        for ( long int ex = 0; ex < stab->nelts; ++ex ) {
          long int e = stab->elts[ex];
          long int ox   = stab->tcycls[3*e];
          long int init = stab->tcycls[3*e+1];
          long int len  = stab->tcycls[3*e+2];
          fmpz_add_ui(t, d, (unsigned long int)ox);
          fmpz_mod_ui(t, t, (unsigned long int)len);
          long int ix = fmpz_get_si(t) + init;
          iselem_    &= (stab->sperm[e]==stab->epsperm[ix]);
          isnegelem_ &= (stab->epsperm[e]==stab->sperm[ix]);
        }
        *iselem    = iselem_;
        *isnegelem = isnegelem_;

        fmpz_clear(t);
        return;
      }

    } else { /* c < 0 */
      if ( fmpz_is_zero(d) ) { /* d == 0 */
        /* g = -S = \epsilon S' and -g = S = S' \epsilon. */
        long int iselem_    = 1;
        long int isnegelem_ = 1;
        for ( long int ex = 0; ex < stab->nelts; ++ex ) {
          long int e = stab->elts[ex];
          iselem_    &= (stab->epsperm[e]==stab->sperm[e]);
          isnegelem_ &= (stab->sperm[e]==stab->epsperm[e]);
        }
        *iselem    = iselem_;
        *isnegelem = isnegelem_;

        return;

      } else { /* d != 0 */
        fmpz_t t;
        fmpz_init(t);

        /* g = \epsilon S' T^-d and -g = S' \epsilon T^-d. */
        long int iselem_    = 1;
        long int isnegelem_ = 1;
        for ( long int ex = 0; ex < stab->nelts; ++ex ) {
          long int e = stab->elts[ex];
          long int ox   = stab->tcycls[3*e];
          long int init = stab->tcycls[3*e+1];
          long int len  = stab->tcycls[3*e+2];
          fmpz_sub_ui(t, d, (unsigned long int)ox);
          fmpz_neg(t, t);
          fmpz_mod_ui(t, t, (unsigned long int)len);
          long int ix = fmpz_get_si(t) + init;
          iselem_    &= (stab->epsperm[e]==stab->sperm[ix]);
          isnegelem_ &= (stab->sperm[e]==stab->epsperm[ix]);
        }
        *iselem    = iselem_;
        *isnegelem = isnegelem_;

        fmpz_clear(t);
        return;
      }
    }
  }


  /* Normalize element to a,c > 0 and record potential epsilon factors.
   * In order to fix negative a, we multiply from the left with -\epsilon.
   * In order to fix negative c, we multiply from the left with \epsilon. */
  fmpq_t ac;
  fmpq_init(ac);
  fmpq_set_fmpz_frac(ac, a, c);

  long int flagmulnegeps, flagmuleps;
  if ( fmpz_cmp_si(a,0) == -1 ) { /* a < 0 */
    if ( fmpz_cmp_si(c,0) == -1 ) { /* c < 0 */
      flagmulnegeps = 1;
      flagmuleps    = 1;
    } else { /* c > 0 */
      flagmulnegeps = 1;
      flagmuleps    = 0;
      fmpq_neg(ac, ac);
    }

  } else { /* a > 0 */
    if ( fmpz_cmp_si(c,0) == -1 ) { /* c < 0 */
      flagmulnegeps = 0;
      flagmuleps    = 1;
      fmpq_neg(ac, ac);
    } else { /* c > 0 */
      flagmulnegeps = 0;
      flagmuleps    = 0;
    }
  }

  /* Get partial fraction expansion of abs(a/c). */
  long int cfrac_lenbd = fmpq_cfrac_bound(ac);
  if ( cfrac_lenbd > stab->cfrac_len ) {
    if ( stab->cfrac )
      _fmpz_vec_clear(stab->cfrac, stab->cfrac_len);
    stab->cfrac = _fmpz_vec_init(cfrac_lenbd);
    stab->cfrac_len = cfrac_lenbd;
  }
  long int cfrac_len = fmpq_get_cfrac(stab->cfrac, ac, ac, cfrac_lenbd);

  fmpq_clear(ac);


  /* The continued fraction expansion gives us a sequence of integers h_i such that
   * [abs(a) b'; abs(c) d'] = [h_0 1; 1 0] ... [a_{cfrac_len} 1; 1 0].
   * Moreover, for any continued fraction expansion of length at least one, We have 
   * 0 < b' <= abs(a) and 0 \le d' < abs(c). */

  fmpz_t k;
  fmpz_init(k);
  if ( !(cfrac_len & 1) ^ flagmuleps ^ flagmulnegeps ) {
    /* In this case the the matrix
     *   \epsilon^flagmuleps (-\epsilon)^flagmulnegeps
     *   [abs(a) b'; abs(c) d']
     * lies in SL2Z. We set
     *   k = fdiv(sgn(c)d, abs(c)) = fdiv(d,c).
     * Since 0 \le d' < abs(c), we have
     *   [a b; c d] 1
     * =
     *   \epsilon^flagmuleps (-\epsilon)^flagmulnegeps
     *   [h_0 1; 1 0] ... [a_{cfrac_len} 1; 1 0] T^k 1 */
    fmpz_fdiv_q(k, d, c);
    fmpz_mod(k, k, stab->tcycllcm);

  } else {
    /* In this case the the matrix
     *   \epsilon^flagmuleps (-\epsilon)^flagmulnegeps
     *   [abs(a) b'; abs(c) d']
     *   \epsilon
     * lies in SL2Z. We set
     *   k = cdiv(sgn(c)d, abs(c)) = cdiv(d,c).
     * Since -abs(c) < -d' \le 0, we have
     *   [a b; c d] 1
     * =
     *   \epsilon^flagmuleps (-\epsilon)^flagmulnegeps
     *   [h_0 1; 1 0] ... [a_{cfrac_len} 1; 1 0] \epsilon T^k 1 */
    fmpz_cdiv_q(k, d, c);
    fmpz_mod(k, k, stab->tcycllcm);
  }


  fmpz_t t;
  fmpz_init(t);
  long int iselem_    = 1;
  long int isnegelem_ = 1;
  for ( long int ex = 0; ex < stab->nelts; ++ex ) {
    long int e = stab->elts[ex];

    /* ix = T^k e */
    long int ox   = stab->tcycls[3*e];
    long int init = stab->tcycls[3*e+1];
    long int len  = stab->tcycls[3*e+2];
    fmpz_add_ui(t, k, (unsigned long int)ox);
    fmpz_mod_ui(t, t, (unsigned long int)len);
    long int ix = fmpz_get_si(t) + init;

    if ( !( !(cfrac_len & 1) ^ flagmuleps ^ flagmulnegeps ) ) {
      /* See the above comments on the computation of k. */
      /* ix = epsilon ix */
      ix = stab->epsperm[ix];
    }

    for ( long int cx = cfrac_len-1; cx >= 0; --cx ) {
      /* We apply [cfrac[ix] 1; 1 0] = T^cfrac[ix] S'. */
      /* ix = S' ix */
      ix = stab->sperm[ix];
      /* ix = T^cfrac[cx] ix */
      ox   = stab->tcycls[3*ix];
      init = stab->tcycls[3*ix+1];
      len  = stab->tcycls[3*ix+2];
      fmpz_add_ui(t, stab->cfrac+cx, (unsigned long int)ox);
      fmpz_mod_ui(t, t, (unsigned long int)len);
      ix = fmpz_get_si(t) + init;
    }

    /* gjx = \epsilon^flagmuleps (-\epsilon)^flagmulnegeps ix */
    /* gnegjx = -I \epsilon^flagmuleps (-\epsilon)^flagmulnegeps ix */
    if ( flagmuleps ) {
      if ( flagmulnegeps ) {
        /* gjx = -I ix */
        /* gnegjx = ix */
        iselem_    &= (stab->negperm[e]==ix);
        isnegelem_ &= (e==ix);
      } else {
        /* gjx = \epsilon ix */
        /* gnegjx = -\epsilon ix */
        ix = stab->epsperm[ix];
        iselem_    &= (e==ix);
        isnegelem_ &= (stab->negperm[e]==ix);
      }
    } else {
      if ( flagmulnegeps ) {
        /* gjx = -\epsilon ix */
        /* gnegjx = \epsilon ix */
        ix = stab->epsperm[ix];
        iselem_    &= (stab->negperm[e]==ix);
        isnegelem_ &= (e==ix);
      } else {
        /* gjx = ix */
        /* gnegjx = -I ix */
        iselem_    &= (e==ix);
        isnegelem_ &= (stab->negperm[e]==ix);
      }
    }
  }
  *iselem    = iselem_;
  *isnegelem = isnegelem_;

  fmpz_clear(t);
  fmpz_clear(k);

  return;
}
