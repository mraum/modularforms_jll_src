/*
    Copyright (C) 2021  Martin Raum

    This file is a part of ModularForms_jll_src.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include "flint/fmpz.h"
#include "perm_stab_gl2z.h"

void
perm_stab_gl2z_init(
    perm_stab_gl2z_t stab,
    const long int * elts,
    const long int nelts,
    const long int * epsperm,
    const long int * negperm,
    const long int * sperm,
    const long int permsize,
    const long int * tcycls,
    const fmpz_t tcycllcm
    )
{
  stab->elts = (long int *)flint_malloc(sizeof(long int) * (nelts+6*permsize));
  memcpy(stab->elts, elts, sizeof(long int) * nelts);

  stab->nelts = nelts;

  stab->epsperm = stab->elts + nelts;
  memcpy(stab->epsperm, epsperm, sizeof(long int) * permsize);

  stab->negperm = stab->elts + nelts + permsize;
  memcpy(stab->negperm, negperm, sizeof(long int) * permsize);

  stab->sperm = stab->elts + nelts + 2*permsize;
  memcpy(stab->sperm, sperm, sizeof(long int) * permsize);

  stab->permsize = permsize;

  stab->tcycls = stab->elts + nelts + 3*permsize;
  memcpy(stab->tcycls, tcycls, sizeof(long int) * 3*permsize);

  fmpz_init_set(stab->tcycllcm, tcycllcm);

  stab->cfrac = NULL;
  stab->cfrac_len = 0;
}
