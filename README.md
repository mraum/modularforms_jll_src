# Build instructions (6/11-2021)

Install FLINT and Arb in whatever way you prefer (at the time of writing we used
Arb at version 2.21.1-1 and FLINT at version 2.8.3-1).

Then run

```sh
./configure --with-flint=/FLINT_PATH --with-arb=/ARB_PATH
```

where `/FLINT_PATH/lib/` contains `libflint.so` and `/FLINT_PATH/include` cont-
ains the directory `flint`; and where `/ARB_PATH/lib` contains `libarb.so` and
`/ARB_PATH/include` contains `arb.h`.

Then run

```sh
$ make
# make install
```

Unless you've set the prefix for `./configure`, the build script will place
`libmodularforms_jll_src.so` at `/usr/local/lib/` and the include files (`misc.h`,
`arb_wrappers.h`, and `perm_stab_gl2z.h`) at
`/usr/local/include/modularforms_jll_src`.

Finally, make sure that `libmodularforms_jll_src.so` can be found by the linker
by setting `LD_LIBRARY_PATH` to include the corresponding path. You can do this
with an `export` or by adding a file to `/etc/ld.so.conf.d/` and running
`ldconfig`.
